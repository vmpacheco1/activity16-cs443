Feature: Customer Discount Calculation
  Scenario: Customer with less than 10 orders
    Given a customer has 5 past orders
    When the customer makes a purchase of $500
    Then the discount should be 0 percent

  Scenario: Customer with 15 orders
    Given a customer has 15 past orders
    When the customer makes a purchase of $200
    Then the discount should be 5 percent

  Scenario: Customer with 50 orders and large purchase
    Given a customer has 50 past orders
    When the customer makes a purchase of $1200
    Then the discount should be 11 percent
