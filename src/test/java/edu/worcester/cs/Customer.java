package test.java.edu.worcester.cs;

public class Customer {
    private int pastOrderCount;

    public Customer(int pastOrderCount) {
        if (pastOrderCount < 0) {
            throw new IllegalArgumentException("pastOrderCount must be greater than zero!");
        }
        this.pastOrderCount = pastOrderCount;
    }

    public int getPastOrderCount() {
        return pastOrderCount;
    }

    public int calculateOrderDiscount(double orderTotal) {
        int percent = 0;
        if (pastOrderCount >= 10 && pastOrderCount <= 40) {
            percent = 5;
        } else if (pastOrderCount > 40) {
            percent = 10;
        }
        if (orderTotal > 1000.00) {
            percent += 1;
        }
        pastOrderCount++;
        return percent;
    }
}
